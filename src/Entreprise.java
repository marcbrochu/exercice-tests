import java.sql.SQLException;
import java.util.List;

public class Entreprise {
    
    private ConnexionDB db;
    
    public Entreprise() throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
        connexion("db bidon");
    }

    public void connexion(String uri) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
        db = new ConnexionDB(uri);
    }

    public void deconnexion() {
    	try {
			this.db.close();
		} catch (SQLException e) {
			// Récupération spécifique
		}
    }

    public void ajouterEmploye(int code, String nom, String prenom) throws SQLException {
    	Employe e = new Employe(code, nom, prenom);
    	db.insert(e);
    }

    public void supprimerEmploye(int code) throws SQLException {
    	db.delete(code);
    }

    public String toString(int code) throws Exception {
    	Employe e = db.select(code);
    	return toString(e);
    }
    
    private String toString(Employe e) {
        return "Employe " + e.ID + " : " + e.prenom + " " + e.nom + "\n";
    }

    public String toString() {
        List<Employe> es = null;
		try {
			es = db.selectAll();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
        String output = "";
        for (Employe e : es) {
            output += toString(e);
        }
        return output;
    }
}
